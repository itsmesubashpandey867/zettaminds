import './App.css';
import Footer from './components/footer/Footer';
import HeaderNav from './components/header/HeaderNav';
import HomeRoute from './components/router/HomeRoute';

function App() {
  return (
    <div className="App">
      <HeaderNav />
      <HomeRoute />
      <Footer />
    </div>
  );
}

export default App;
